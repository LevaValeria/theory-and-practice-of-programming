package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InterpreterTest {

    @Test
    void test1() throws Exception {
        Lexer lexer = new Lexer("5 13 7 - *");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(30,result);
    }

    @Test
    void test2() throws Exception {
        Lexer lexer = new Lexer("3 10 15 - *");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(-15,result);
    }

    @Test
    void test3() throws Exception {
        Lexer lexer = new Lexer("2 3 / ");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(1.5,result);
    }

    @Test
    void test4() throws Exception {
        Lexer lexer = new Lexer("3 6 2 * + - ");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(9,result);
    }

    @Test
    void test5() throws Exception {
        Lexer lexer = new Lexer("+ / 3 6 2");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(9,result);
    }

    @Test
    void test6() throws Exception {
        Lexer lexer = new Lexer("3 6 * 2 / 3");
        Interpreter interp = new Interpreter(lexer);
        float result = interp.expr();
        assertEquals(9,result);
    }
}