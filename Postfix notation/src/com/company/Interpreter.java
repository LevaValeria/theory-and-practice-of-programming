package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by valeria on 09/11/2018.
 */
public class Interpreter {

    private Token currentToken;
    private Lexer lexer;
    private LinkedList stk;


    public Interpreter(Lexer lexer) throws Exception {
        this.lexer = lexer;
        currentToken = this.lexer.nextToken();
        stk = new LinkedList();
    }

    public void checkTokenType(TokenType type) throws Exception {
        if (currentToken.getType() == type) {
            currentToken = lexer.nextToken();
        } else {
            throw new Exception("Parser error!");
        }

    }

    private float factor() throws Exception {
        Token token = currentToken;
        if (token.getType().equals(TokenType.INTEGER)) {
            checkTokenType(TokenType.INTEGER);
            return Float.parseFloat(token.getValue());
        } else if (token.getType().equals(TokenType.FLOAT)) {
            checkTokenType(TokenType.FLOAT);
            return Float.parseFloat(token.getValue());
        }
        throw new Exception("Factor error");
    }

    public float expr() throws Exception {
        stk.addFirst(factor());

        List<TokenType> ops = Arrays.asList(TokenType.INTEGER, TokenType.FLOAT);
        while (ops.contains(currentToken.getType())) {
            List<TokenType> oper = Arrays.asList(TokenType.PLUS, TokenType.MINUS, TokenType.DIV, TokenType.MUL, TokenType.EXP);
            stk.addLast(factor());
            while (oper.contains(currentToken.getType())) {
                Token token = currentToken;
                if(stk.size() >= 2){
                    if (token.getType() == TokenType.PLUS ) {
                        checkTokenType(TokenType.PLUS);
                        stk.addFirst(Float.parseFloat(stk.removeLast().toString()) + Float.parseFloat(stk.removeLast().toString()));
                    } else if (token.getType() == TokenType.MINUS) {
                        checkTokenType(TokenType.MINUS);
                        stk.addFirst(Float.parseFloat(stk.remove(stk.size()-2).toString()) - Float.parseFloat(stk.removeLast().toString()));
                    } else if (token.getType() == TokenType.MUL) {
                        checkTokenType(TokenType.MUL);
                        stk.addFirst(Float.parseFloat(stk.removeLast().toString()) * Float.parseFloat(stk.removeLast().toString()));
                    } else if (token.getType() == TokenType.EXP) {
                        checkTokenType(TokenType.EXP);
                        stk.addFirst(Math.pow(Float.parseFloat(stk.removeLast().toString()), Float.parseFloat(stk.removeLast().toString())));
                    } else if (token.getType() == TokenType.DIV) {
                        if (Float.parseFloat(stk.get(1).toString()) == 0) {
                            throw new Exception("Division by zero!");
                        } else {
                            checkTokenType(TokenType.DIV);
                            stk.addFirst(Float.parseFloat(stk.removeLast().toString()) / Float.parseFloat(stk.removeLast().toString()));
                        }
                    }
                } else if (stk.size() < 2) {
                    throw new Exception("Arity error!");
                }
            }

        }
        if (stk.size() > 1) {
            throw new Exception("Arity error!");
        }
        return Float.parseFloat(stk.removeLast().toString());
    }


    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Interpreter interp;
        Lexer lexer;
        while (true) {
            System.out.print("in> ");
            String text = reader.readLine();
            if (text.equals("exit") | text.length() < 0) {
                break;
            }
            lexer = new Lexer(text);
            System.out.print("out> ");
            try {
                interp = new Interpreter(lexer);
                float result = interp.expr();
                System.out.println(result);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
