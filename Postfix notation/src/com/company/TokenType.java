package com.company;

/**
 * Created by valeria on 09/11/2018.
 */
public enum TokenType {
    PLUS,
    MINUS,
    DIV,
    MUL,
    EXP,
    INTEGER,
    FLOAT,
    EOF
}
