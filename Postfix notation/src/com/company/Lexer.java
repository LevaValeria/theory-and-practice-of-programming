package com.company;

/**
 * Created by valeria on 09/11/2018.
 */
public class Lexer {
    private String text;
    private int pos = 0;
    private Character currentChar;

    public Lexer(String text) {
        this.text = text;
        currentChar = text.charAt(pos);
    }

    public void forward() {
        pos += 1;
        if (pos > text.length() - 1) {
            currentChar = null;
        } else {
            currentChar = text.charAt(pos);
        }
    }

    public void skip() {
        while (currentChar != null && Character.isSpaceChar(currentChar)) {
            forward();
        }
    }


    public Token number() {
        String result = "";
        while (currentChar != null && (Character.isDigit(currentChar) | currentChar == '.')) {
            result += currentChar;
            forward();
        }
        if (result.contains(".")) {
            return new Token(TokenType.FLOAT, result);
        } else {
            return new Token(TokenType.INTEGER, result);
        }
    }

    public Token nextToken() throws Exception {
        while (currentChar != null) {
            if (Character.isSpaceChar((currentChar))) {
                skip();
                continue;
            }
            if (Character.isDigit(currentChar) | currentChar == '.') {
                return number();
            }

            if (currentChar == '+') {
                forward();
                return new Token(TokenType.PLUS, "" + "+");
            }
            if (currentChar == '-') {
                forward();
                return new Token(TokenType.MINUS, "" + "-");
            }
            if (currentChar == '*') {
                forward();
                return new Token(TokenType.MUL, "" + "*");
            }
            if (currentChar == '/') {
                forward();
                return new Token(TokenType.DIV, "" + "/");
            }

            if (currentChar == '^') {
                forward();
                return new Token(TokenType.EXP, "" + "^");
            }


            throw new Exception("Unknown token!");
        }
        return new Token(TokenType.EOF, null);
    }

}


