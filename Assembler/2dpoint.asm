%macro pushd 0
    push eax
    push ebx
    push ecx
    push edx
%endmacro

%macro popd 0
    pop edx
    pop ecx
    pop ebx
    pop eax
%endmacro

%macro print 2
    pushd
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
    popd
%endmacro

%macro dprint 0;
    pushd
    mov ecx, 10
    mov bx, 0

%%_divide:
     xor edx, edx
     div ecx
     push dx
     inc bx
     test eax, eax
     jnz %%_divide

%%_digit:
    pop ax
    add ax, '0'
    mov [reminder], ax
    print 1, reminder
    dec bx
    cmp bx, 0
    jg %%_digit
    popd
%endmacro

%macro sqrt 1
pushd
    mov eax, [%1]
    mov ebx, 2
    cwd
    div ebx 
    mov ecx, eax 
    mov eax, [%1]
    cwd
    div ecx
    add eax, ecx
    cwd
    div ebx 
    push eax 
    
%%_while:
    sub ecx, eax 
    abs ecx 
    cmp eax, 1
    jg %%_g

%%_l:
    pop eax
    jmp %%_end
%%_g:
    pop eax
    mov ecx, eax  
    mov eax, [%1]
    cwd
    div ecx 
    add eax, ecx
    cwd
    div ebx
    push eax
    jmp %%_while
popd
%%_end:

%endmacro

%macro abs 1
    mov eax, %1
    cmp eax, 0
    jl %%_l

%%_g:
    jmp %%_end
%%_l:
    mov ebx, eax
    xor eax, eax
    sub eax, ebx
%%_end:

%endmacro

section .text

global _start

_start:

mov ebx, 0
mov eax, 0
mov [max], eax
_forx:
    mov ecx, ebx
    add ecx, 4
    _fory:
        mov eax, 0
        add al, [x + ecx]
        add dl, [x + ebx]
        sub al, dl
        imul al
        push eax
        mov eax, 0
        xor dl, dl
        add al, [y + ecx]
        add dl, [y + ebx]
        sub al, dl
        imul al
        mov dl, al
        pop eax
        add al, dl

        mov [res], eax 
        cmp eax, [max]
        jge _gr
        _le:
            jmp _en
        _gr:
            mov [max], eax
        _en:

        add ecx, 4
        xor eax, eax
        xor dl, dl
        cmp ecx, ylen
        jl _fory
    add ebx, 4
    cmp ebx, xlen
    jl _forx


    sqrt max
    print len, message
    dprint
    print nlen, newline
    
    mov     eax, 1
    int     0x80

   
section .data
    x dd 10, 2, 5, 9, 11
    xlen equ $ - x - 1
    y dd 2, 3, 6, 10, 12
    ylen equ $ - y 
    max dd 0
    res dd 0
    result dd 25
    reslen equ $ - result
    message db "Max:"
    len equ $ - message
    newline db  0xA, 0xD
    nlen equ $ - newline

section .bss
    reminder resb 1 
