vowels = ('a', 'e', 'i', 'o', 'u', 'y')
consonants = ('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
              'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z')
inp = input('Enter the word:: ').strip().lower()
a = sum(1 for x in inp if x in vowels)
b = sum(1 for x in inp if x in consonants)
print('Vowels:', a,'Сonsonants:', b)