import itertools
import numpy as np
with open('signals.bin', 'rb') as inp:
    lst = inp.read()

b = []
c = {'g1': [], 'g2': [], 'g3': []}
d = []
f = {0: [], 1: []}
h = {0: [], 1: []}
g = {0: [], 1: []}

for i in range(2, len(lst), 3):
    b.append([int(i) for i in "{0:08b}".format(lst[i])])

for i in b:
    c['g1'].append(i[0])
    c['g2'].append(i[3])
    c['g3'].append(i[5])

for key, igroup in itertools.groupby(c['g1']):
    f[key].append(len(list(igroup)))
for key, igroup in itertools.groupby(c['g2']):
    h[key].append(len(list(igroup)))
for key, igroup in itertools.groupby(c['g3']):
    g[key].append(len(list(igroup)))

print(round(1/((np.median(f[0])+np.median(f[1]))*0.001)))
print(round(1/((np.median(h[0])+np.median(h[1]))*0.001)))
print(round(1/((np.median(g[0])+np.median(g[1]))*0.001)))



