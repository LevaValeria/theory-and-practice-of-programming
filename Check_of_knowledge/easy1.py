lst = ['+', '-', '*', '/']
result = 0.0
try:
    num1, num2, operator = float(input('Enter 1 number:')), float(input('Enter 2 number:')), \
                       input('Enter operator (+, -, *, /): ').strip()
    if operator in lst:
        if operator == lst[0]:
            result = num1 + num2
        elif operator == lst[1]:
            result = num1 - num2
        elif operator == lst[2]:
            result = num1 * num2
        elif operator == lst[3]:
            result = num1 / num2
        print('The result of calculations: ', result)
    else:
        print('Incorrect operator.')
except ValueError:
    print('Not a number. Try again.')
except ZeroDivisionError:
    print('Division by zero.')