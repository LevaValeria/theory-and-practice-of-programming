def similar(first, second):
    a, b, c = set(first.strip().lower()), set(second.strip().lower()), 0.0
    lst = a.intersection(b)
    return len(lst)/(len(a)+len(b)-len(lst))

print("The coefficient of similarity:", similar(input("Enter 1 word:"), input("Enter 2 word:")))

