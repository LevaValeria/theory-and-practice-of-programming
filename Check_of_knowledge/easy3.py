opr = ('\"', ':', '.', ',', '?', '!', ';', '\n', ' - ')
with open('kaffka.txt', 'r') as inp:
    lst = inp.read()
for i in opr:
    lst = lst.replace(i, '')
mxword = max(set(lst.split()), key=len)
print("The longest word:", mxword)

