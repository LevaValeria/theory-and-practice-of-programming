package tests;

import com.company.LengthRule;
import com.company.Validator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by valeria on 29/09/2018.
 */
class ValidatorTest {
    public static Validator validator;

    @BeforeAll
    public static void initializeValidator(){
        validator = new Validator();
        validator.registerRule(new LengthRule(10));
    }

    @Test
    public void weakPassword(){
        assertEquals(false,validator.validate("!@AS123"));

    }

    @Test
    public void strongPassword(){
        assertEquals(true,validator.validate("!@AS123"));

    }

}