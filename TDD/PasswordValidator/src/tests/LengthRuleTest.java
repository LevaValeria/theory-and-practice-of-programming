package tests;

import com.company.LengthRule;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by valeria on 29/09/2018.
 */
public class LengthRuleTest {
    @Test
    public void insufficientLength(){
        LengthRule rule = new LengthRule(5);
        assertEquals(false, rule.check("abs"));

    }

    @Test
    public void sufficientLength(){
        LengthRule rule = new LengthRule(3);
        assertEquals(true, rule.check("abs"));

    }
}
