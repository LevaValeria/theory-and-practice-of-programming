package com.company;

/**
 * Created by valeria on 29/09/2018.
 */
public interface Rule {

    boolean check(String password);
}
