package com.company;

/**
 * Created by valeria on 29/09/2018.
 */
public class LengthRule implements Rule{

    private int allowableLength = 1;

    public LengthRule(int allowableLength){
        this.allowableLength = allowableLength;
    }

    @Override
    public boolean check(String password) {
        if (password.length()>=allowableLength){
            return true;
        }
        return false;
    }
}
