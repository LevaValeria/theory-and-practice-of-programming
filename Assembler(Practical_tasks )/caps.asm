%macro pushd 0
    push eax
    push ebx
    push ecx
    push edx
%endmacro

%macro popd 0
    pop edx
    pop ecx
    pop ebx
    pop eax
%endmacro

%macro print 2
    pushd
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
    popd
%endmacro

%macro dprint 0;
    pushd
    mov ecx, 10
    mov bx, 0

%%_divide:
     xor edx, edx
     div ecx
     push dx
     inc bx
     test eax, eax
     jnz %%_divide

%%_digit:
    pop ax
    add ax, '0'
    mov [reminder], ax
    print 1, reminder
    dec bx
    cmp bx, 0
    jg %%_digit

%endmacro

%macro caps 2
pushd
mov ebx, 0
%%_while:
    add al, [%1 + ebx]
    cmp al, 97
    jl %%_end 
    cmp al, 122
    jg %%_end
    sub al, 20h
%%_end:
    add [reminder2], al
    mov [%1 + ebx], al
    mov al, 0
    mov [reminder2], al
    inc ebx
    cmp ebx, %2
    jl %%_while

popd
%endmacro
section .text

global _start

_start:

    print arlen, array
    caps array, arlen
    print arlen, array
    print nlen, newline
    
    print len, message
    print nlen, newline
    
    mov     eax, 1
    int     0x80

   
section .data
    array db "hell"
    arlen equ $ - array
    message db "Done"
    len equ $ - message
    newline db  0xA, 0xD
    nlen equ $ - newline

section .bss
    reminder resb 1 
    reminder2 resb 1 
