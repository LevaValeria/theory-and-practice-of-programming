package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by valeria on 27/10/2018.
 */
//public class Interpreter {
//
//    private Token currentToken;
//    private Lexer lexer;
//
//
//    public Interpreter(Lexer lexer) throws Exception {
//        this.lexer = lexer;
//        currentToken = this.lexer.nextToken();
//    }
//
//
//
//    public void checkTokenType(TokenType type) throws Exception {
//        if(currentToken.getType() == type){
//            currentToken = lexer.nextToken();
//        }
//        else {
//            throw new Exception("Parser error!");
//        }
//
//    }
//
//    private float factor() throws Exception {
//        Token token = currentToken;
//        if(token.getType().equals(TokenType.INTEGER)){
//            checkTokenType(TokenType.INTEGER);
//            return Float.parseFloat(token.getValue());
//        }
//        else if(token.getType().equals(TokenType.FLOAT)){
//            checkTokenType(TokenType.FLOAT);
//            return Float.parseFloat(token.getValue());
//        }
//        else if(token.getType().equals(TokenType.LPAREN)){
//            checkTokenType(TokenType.LPAREN);
//            float result = expr();
//            checkTokenType(TokenType.RPAREN);
//            return result;
//        }
//    throw new Exception("Factor error");
//    }
//
//
//
//    public float term() throws Exception {
//        float result = factor();
//        List<TokenType>  ops = Arrays.asList(TokenType.DIV, TokenType.MUL, TokenType.EXP);
//        while (ops.contains(currentToken.getType())){
//            Token token = currentToken;
//            if (token.getType() == TokenType.DIV){
//                checkTokenType(TokenType.DIV);
//                result /= factor();
//
//            }
//            else if (token.getType() == TokenType.MUL){
//                checkTokenType(TokenType.MUL);
//                result *= factor();
//            }
//            else if (token.getType() == TokenType.EXP){
//                checkTokenType(TokenType.EXP);
//               result =  Float.parseFloat(Double.toString(Math.pow(result, factor())));
//            }
//        }
//        return result;
//    }
//
//    public float expr() throws Exception {
//
//        List<TokenType>  ops = Arrays.asList(TokenType.PLUS, TokenType.MINUS);
//        float result = term();
//        while (ops.contains(currentToken.getType())){
//            Token token = currentToken;
//            if (token.getType() == TokenType.PLUS){
//                checkTokenType(TokenType.PLUS);
//                result += term();
//
//            }
//            if (token.getType() == TokenType.MINUS){
//                checkTokenType(TokenType.MINUS);
//                result -= term();
//            }
//        }
//        return result;
//    }
//
//    public static void main(String[] args) throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        Interpreter interp;
//        Lexer lexer;
//        while (true){
//            System.out.print("in> ");
//            String text = reader.readLine();
//            if (text.equals("exit")| text.length() < 0){
//                break;
//            }
//            lexer = new Lexer(text);
//
//            System.out.print("out> ");
//            try {
//                interp = new Interpreter(lexer);
//                float result = interp.expr();
//                System.out.println(result);
//            } catch (Exception e) {
//                System.out.println(e.getMessage());
//            }
//        }
//    }
//
//}
