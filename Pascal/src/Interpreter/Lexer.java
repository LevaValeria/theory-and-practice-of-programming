package Interpreter;



/**
 * Created by valeria on 29/10/2018.
 */
public class Lexer {

    private String text;
    private int pos = 0;
    private Character currentChar;

    public Lexer(String text){
        this.text = text;
        currentChar = text.charAt(pos);
    }

    public void forward(){
        pos += 1;
        if(pos > text.length() - 1){
            currentChar =  null;
        }
        else{
            currentChar = text.charAt(pos);
        }
    }

    public void skip(){
        while (currentChar != null && Character.isSpaceChar(currentChar)){
            forward();
        }
    }

    public Character next(){
        Character newChar;
        int newPos = pos + 1;
        if ( newPos > text.length() - 1 ){
            newChar = null;
        }
        else {
            newChar = text.charAt(newPos);
        }
        return newChar;
    }

    public Token number() throws Exception {
        String result = "";
        while (currentChar != null && (Character.isDigit(currentChar) | currentChar == '.')){
            result += currentChar;
            forward();
        }
        if(result.contains(".") && result.length() > 1){
            return new Token(TokenType.FLOAT,  result);
        }
        else {
            return new Token(TokenType.INTEGER,  result);
        }
    }



    public Token alphabetic(){
        String result = "";
        while (currentChar != null && (Character.isLetterOrDigit(currentChar))){
            result += currentChar;
            forward();
        }
        if(result.equals("BEGIN")){
            return new Token(TokenType.BEGIN, result);
        }
        else if(result.equals("END")){
            return new Token(TokenType.END, result);
        }
        else {
            return new Token(TokenType.VAR, result);
        }

    }

    public Token nextToken() throws Exception {
        while (currentChar != null ) {
            if (currentChar == '.'){
                forward();
                return new Token(TokenType.DOT,".");
            }
            if (Character.isSpaceChar((currentChar))){
                skip();
                continue;
            }
            if (Character.isDigit(currentChar) | currentChar == '.') {
                return number();
            }
            if (Character.isLetterOrDigit(currentChar)){
                return alphabetic();
            }
            if (currentChar == '+') {
                forward();
                return new Token(TokenType.PLUS,  "+");
            }
            if (currentChar == '-') {
                forward();
                return new Token(TokenType.MINUS,  "-");
            }
            if (currentChar == '*') {
                forward();
                return new Token(TokenType.MUL,  "*");
            }
            if (currentChar == '/') {
                forward();
                return new Token(TokenType.DIV, "/");
            }
            if (currentChar == '(') {
                forward();
                return new Token(TokenType.LPAREN,  "(");
            }
            if (currentChar == ')') {
                forward();
                return new Token(TokenType.RPAREN, ")");
            }
            if (currentChar == '^') {
                forward();
                return new Token(TokenType.EXP,   "^");
            }

            if (currentChar == ';'){
                forward();
                return new Token(TokenType.SEMICOLON,";");
            }
            if (currentChar == ':' && next() == '='){
                forward();
                forward();
                return new Token(TokenType.ASSIGNS,":=");
            }

            throw new Exception("Unknown token!");
        }
        return new Token(TokenType.EOF, null);
    }


}
