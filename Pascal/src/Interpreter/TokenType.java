package Interpreter;



/**
 * Created by valeria on 27/10/2018.
 */
public enum TokenType {
    PLUS,
    MINUS,
    DIV,
    MUL,
    LPAREN,

    BEGIN,
    END,
    SEMICOLON,
    ASSIGNS,
    DOT,
    VAR,


    RPAREN,
    EXP,
    INTEGER,
    FLOAT,
    EOF
}
