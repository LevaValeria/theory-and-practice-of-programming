package Server;

public class Const {
    public static final String USER_TABLE = "users";
    public static final String USERS_ID = "id";
    public static final String USERS_LOGIN = "login";
    public static final String USERS_PASSWORD = "password";
    public static final String USERS_MAINDIR = "mainDir";
    public static final String USERS_CONFIRM = "confirmUsers";

}
