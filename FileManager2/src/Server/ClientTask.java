package Server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

public class ClientTask implements Runnable{
    private Socket clientSock;
    private PrintWriter out;
    private BufferedReader in;
    private String mainPath;


    public ClientTask(Socket client, String mainPath) throws IOException {
        this.clientSock = client;
        out = new PrintWriter(clientSock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSock.getInputStream()));
        this.mainPath = mainPath;
    }

    @Override
    public void run() {
        JSONParser parser = new JSONParser();
        System.out.println("Opened connection to " + clientSock.getRemoteSocketAddress());
        while(!clientSock.isOutputShutdown()) {
            String data = null;
            JSONObject answer = new JSONObject();
            try {
                data = in.readLine();
                if (data == null) {
                    break;
                }
                JSONObject message = (JSONObject) parser.parse(data);

                    if (message.get("action").equals("login")) {

                        try {
                            String login = message.get("login").toString();
                            String password = message.get("password").toString();
                            ResultSet resultSet = loginUser(login,password);
                            if (resultSet != null){
                                answer.put("result", "login");
                                answer.put("mainDir", resultSet.getString("mainDir"));
                            }
                            else {
                                answer.put("result", "error");
                            }
                        } catch (Exception e) {
                            answer.put("error", e.getMessage());
                        }
                    }
                    else if(message.get("action").equals("registration")){
                        try {
                            String login = message.get("login").toString();
                            String password = message.get("password").toString();
                            String result = registUser(login, password);
                            if( result != null ){
                                answer.put("result", "registration");
                                answer.put("mainDir", result);
                            }else {
                                answer.put("result", "error");
                            }
                        } catch (Exception e) {
                            answer.put("error", e.getMessage());
                        }
                    }
                    else {
                        answer.put("error", "Expr not found");
                    }
            }catch (IOException | ParseException e) {
                answer.put("error", "JSON is not valid");
            }

            out.println(answer.toJSONString());
        }
        System.out.println("Closed connection " + clientSock.getRemoteSocketAddress());
        try {
            in.close();
            out.close();
            clientSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ResultSet loginUser(String loginText, String loginPassword) throws SQLException, ClassNotFoundException {
        DataBaseHandler dbHandler = new DataBaseHandler();
        User user = new User(loginText, loginPassword);
        ResultSet result = dbHandler.getUser(user);
        if (result.next()){
            return result;
        }
        else {
            return null;
        }
    }

    private String registUser(String login, String password){
        DataBaseHandler dbHandler = new DataBaseHandler();
        User user = new User(login, password);
        try {
            long unixTimestamp = Instant.now().getEpochSecond();
            File path = new File( mainPath + "/" + login + unixTimestamp);
            path.mkdir();
            dbHandler.registeredUser(user, path.getPath());
            return path.toString();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
