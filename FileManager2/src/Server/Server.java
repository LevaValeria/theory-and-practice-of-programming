package Server;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;



public class Server {
    private ServerSocket serverSock;
    private File mainDir;
    private int port;
    private String mainPath;

    public Server(int port, String mainPath){
        this.port = port;
        this.mainPath = mainPath;
    }


    public void start() throws IOException {
        serverSock = new ServerSocket(port);
        JSONParser parser = new JSONParser();

        mainDir = new File(mainPath);
        if (mainDir.exists() && mainDir.isFile()){
            System.out.println("Error path for main dir!");
        }
        else if (!mainDir.exists()){
            mainDir.mkdir();
        }

        while (true) {
            System.out.println("wait for new connection...");
            ClientTask clientTask = new ClientTask(serverSock.accept(), mainPath);
            System.out.println("Start new thread");
            new Thread(clientTask).start();
        }
    }


    public static void main(String[] args) throws IOException, ParseException {
        Server server = new Server(5004, "tree");
        server.start();
    }
}