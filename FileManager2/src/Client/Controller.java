package Client;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {


    public void openNewScene(Button button, String window, Controller controller){
        Stage stage1 = (Stage) button.getScene().getWindow();
        stage1.toBack();


        FXMLLoader loader = new FXMLLoader(getClass().getResource(window));
        loader.setController(controller);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.showAndWait();
        stage1.close();
    }

}
