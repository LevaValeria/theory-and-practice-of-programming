package Client;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ResourceBundle;

import Server.DataBaseHandler;
import Server.User;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class RegisterController extends Controller{
    private Client client;

    RegisterController(Client client){
        this.client = client;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button registrationButton;

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField confirmPasswordField;

    @FXML
    private Label errorField;

    @FXML
    void initialize() {

        registrationButton.setOnAction(event -> {
            String login = loginField.getText();
            String password = passwordField.getText();
            String passwordConfirm = confirmPasswordField.getText();
            if(!login.equals("") && !password.equals("") && !passwordConfirm.equals("")){
                if(password.equals(passwordConfirm)){
                try {
                    registrationNewUser(login,password);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                }
                else {
                    errorField.setText("Wrong password!");
                }
            }
            else {
                errorField.setText("Empty field!");
            }
        });

        loginButton.setOnAction(event -> {
            openNewScene(loginButton, "/Client/login.fxml", new LoginController(client));
        });
    }

    private void registrationNewUser(String login, String password) throws IOException, ParseException {

        JSONObject response = client.send(client.register(login, password));
        System.out.println(response);
        if(response.get("result").equals("registration")){
            openNewScene(registrationButton, "/Client/main.fxml", new MainController(client, response.get("mainDir").toString()));
        }
        else if (response.get("result").equals("error")){
            errorField.setText("Wrong data!");
        }
    }
}
