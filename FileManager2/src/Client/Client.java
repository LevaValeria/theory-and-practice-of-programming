package Client;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;


    public Client(String host, int port) throws IOException {
        socket = new Socket(host, port);
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public JSONObject send(JSONObject message) throws IOException, ParseException {
        out.println(message.toJSONString());
        String data = in.readLine();

        JSONParser parser = new JSONParser();
        JSONObject resp = (JSONObject) parser.parse(data);
        return resp;
    }

    public JSONObject login(String login, String password){
        JSONObject message = new JSONObject();
        message.put("action", "login");
        message.put("login", login);
        message.put("password", password);
        return message;
    }

    public JSONObject register(String login, String password){
        JSONObject message = new JSONObject();
        message.put("action", "registration");
        message.put("login", login);
        message.put("password", password);
        return message;
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        socket.close();
    }

    public static void pressAnyKeyToContinue(){
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        }
        catch (Exception e){}
    }

    public static void main(String[] args) throws IOException, ParseException {
        Client client = new Client("127.0.0.1", 5001);
        //JSONObject response = client.send("2+2");
        //System.out.println(response);
        //pressAnyKeyToContinue();
        //response = client.send("2+2+2");
        //System.out.println(response);
        client.stop();
    }
}
