package Client;
import java.io.IOException;
import java.net.URL;

import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class LoginController extends Controller{

    private Client client;

    LoginController(Client client){
        this.client = client;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button registrationButton;

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField loginField;
    @FXML

    private Label errorField;

    @FXML
    void initialize() {

        loginButton.setOnAction(event -> {
            String loginText = loginField.getText().trim();
            String loginPassword = passwordField.getText().trim();

            if(!loginText.equals("") && !loginPassword.equals("")){
                try {
                    loginUser(loginText, loginPassword);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                errorField.setText("Empty login or password!");
            }

        });


        registrationButton.setOnAction(event -> {
            RegisterController controller = new RegisterController(client);
            openNewScene(registrationButton, "/Client/register.fxml", controller);
        });
    }

    private void loginUser(String loginText, String loginPassword) throws IOException, ParseException {
        JSONObject response = client.send(client.login(loginText, loginPassword));
        System.out.println(response);
        if(response.get("result").equals("login")){
            openNewScene(loginButton, "/Client/main.fxml", new MainController(client, response.get("mainDir").toString()));
        }
        else if (response.get("result").equals("error")){
            errorField.setText("Wrong login or password!");
        }
    }
}
