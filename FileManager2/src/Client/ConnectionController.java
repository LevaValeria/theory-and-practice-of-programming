package Client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class ConnectionController extends Controller{

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button connectionButton;

    @FXML
    private TextField addressField;

    @FXML
    private TextField portField;

    @FXML
    private Label messageField;

    @FXML
    void initialize() {
        connectionButton.setOnAction(event -> {


            if(addressField.getText().equals("") || portField.getText().equals("")){
             messageField.setText("Fill in the fields");
            }

            String address = addressField.getText().trim();
            int port = Integer.parseInt(portField.getText().trim());


            try {
                Client client = new Client(address, port);
                messageField.setText(client.toString());
                LoginController controller = new LoginController(client);
                openNewScene(connectionButton, "/Client/login.fxml", controller);
            } catch (IOException e) {
                e.printStackTrace();
                messageField.setText("Connection refused!");
            }
        });
    }


}
