package Client;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;

import java.util.*;

public class MainController extends Controller {

    private String mainDir;
    private Client client;

    private HashMap<String, ArrayList> dir = new HashMap<>();
    private HashMap<String, ArrayList> file = new HashMap<>();

    private HashMap<String, HashMap> all = new HashMap<>();

    MainController(Client client, String mainDir){
        this.client = client;
        this.mainDir = mainDir;
    }

    @FXML
    private Button exitButton;

    @FXML
    private TreeTableColumn<String, String> nameColumn;

    @FXML
    private Button registrationButton;

    @FXML
    private TreeTableView<String> table;





    @FXML
    void initialize() throws IOException {
        getListFiles(mainDir);

        TreeItem<String> pathName = new TreeItem<>(mainDir);

        for(Map.Entry<String, ArrayList> entry : dir.entrySet()) {
            String key = entry.getKey();
            TreeItem<String> k = new TreeItem<>(key);
            pathName.getChildren().add(k);
            ArrayList<String> value = entry.getValue();
            for (String s : value){
               k.getChildren().add(new TreeItem<>(s));
            }
        }

        nameColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<String, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<String, String> param) {
                return new SimpleStringProperty(param.getValue().getValue());
            }
        });
        table.setRoot(pathName);
    }

    public void getListFiles(String mainDir) {
        File f = new File(mainDir);
        for (File s : f.listFiles()) {
        if (s.isFile()) {
            if(file.get(mainDir) == null){
            file.put(mainDir, new ArrayList<String>(Arrays.asList(s.getName())));
        }
        else {
            file.get(mainDir).add(s.getName());
            }
        } else if (s.isDirectory()) {
            if(dir.get(mainDir) == null){
                dir.put(mainDir, new ArrayList<String>(Arrays.asList(s.getName())));
            }
            else {
                dir.get(mainDir).add(s.getName());
            }
            getListFiles(s.getAbsolutePath());
        }
        }
        }


}

